import re


class GitLabChangedLinesFinder:
    changed_lines_info_pattern = r'@@ -(\d+),\d+ \+(\d+),\d+ @@'

    def __init__(self, diff_str):
        self.lines = diff_str.split('\n')
        self.added_lines = []
        self.removed_lines = []

    def _get_start_lines_number(self, line) -> tuple[int, int]:
        match = re.match(self.changed_lines_info_pattern, line)
        start_line_with_deleted, start_line_with_added = match.groups()
        return int(start_line_with_added), int(start_line_with_deleted)

    @staticmethod
    def _update_diff_range(line_range, line_number):
        if 'start' in line_range:
            line_range['end'] = line_number
        else:
            line_range['start'] = line_number
            line_range['end'] = line_number
        return line_range

    def _handle_neutral_line(self, added_diff_range, removed_diff_range):
        if added_diff_range:
            self.added_lines.append(added_diff_range)
            added_diff_range = {}
        if removed_diff_range:
            self.removed_lines.append(removed_diff_range)
            removed_diff_range = {}
        return added_diff_range, removed_diff_range

    @classmethod
    def find_changed_lines(cls, diff_str):
        return cls(diff_str)._find_changed_lines()

    def _find_changed_lines(self):
        removed_diff_range = {}
        added_diff_range = {}
        added_line_number = None
        deleted_line_number = None
        for line in self.lines:
            if line.startswith('@@'):
                added_diff_range, removed_diff_range = self._handle_neutral_line(added_diff_range, removed_diff_range)
                added_line_number, deleted_line_number = self._get_start_lines_number(line)
            elif line.startswith('+'):
                added_diff_range = self._update_diff_range(added_diff_range, added_line_number)
                added_line_number += 1
            elif line.startswith('-'):
                removed_diff_range = self._update_diff_range(removed_diff_range, deleted_line_number)
                deleted_line_number += 1
            else:
                added_diff_range, removed_diff_range = self._handle_neutral_line(added_diff_range, removed_diff_range)
                added_line_number += 1
                deleted_line_number += 1
        self._handle_neutral_line(added_diff_range, removed_diff_range)
        return self.added_lines, self.removed_lines


# Example usage:
diff_string = """@@ -11,6 +11,7 @@ from django.db.models import Subquery
 from django.db.models import Sum
 from django.db.models import Value
 from django.db.models import When
+from django.db.models.functions import Round
 from django.db.models.functions import TruncDate
 from polymorphic.query import PolymorphicQuerySet
 
@@ -353,26 +354,27 @@ class TasksQueryset(models.QuerySet):
             ).count() > max_task_under_review_counter
             # or qs.filter(task_list__needs_code_review=False).count() > 3
 
-    def with_total_actual_hours(self, user):
-        from tasks.models import TaskLifecycle
-        time_spent = Subquery(
-            TaskLifecycle.objects.filter(
-                task=OuterRef('pk'),
-                # author_employee=user.employee,
-                to_list__is_development_started=True,
-            ).values('task__pk').annotate(
-                _total=Sum('actual_time'),
-            ).values('_total'),
-        )
+    def with_total_actual_hours(self, employee):
+        # TODO (task id 2701) rename annotated field to _total_actual_hours
         return self.annotate(
-            _total_actual_time=time_spent,
+            _total_actual_time=Round(
+                Sum(
+                    'lifecycles__actual_time',
+                    filter=Q(
+                        lifecycles__author_employee=employee,
+                        lifecycles__to_list__is_development_started=True
+                    )
+                ) / 60.0,
+                1
+            )
         )
 
     def with_estimated_hours(self, employee):
         from tasks.models import TaskRole, EmployeeProject
         estimated_hours = Subquery(
             TaskRole.objects.filter(
-                task_id=OuterRef('id'), role=EmployeeProject.objects.filter(
+                task_id=OuterRef('id'),
+                role=EmployeeProject.objects.filter(
                     employee=employee, project=OuterRef('task__project'),
                 ).values('role_id')[:1],
             ).values('estimated_hours')[:1],
@@ -445,7 +447,7 @@ class EstimatedHourQueryset(models.QuerySet):
         ).annotate(
             _date=TruncDate('tasklifecycle__updated_at'),
         ).distinct().annotate(
-            _total_actual_time=Sum('tasklifecycle__actual_time') / 60,
+            _total_actual_time=Round(Sum('tasklifecycle__actual_time') / 60.0, 1),
         ).order_by('-_date')"""

if __name__ == '__main__':
    added, removed = GitLabChangedLinesFinder.find_changed_lines(diff_string)
    print("Added Rows:", added)
    print("Removed Rows:", removed)
