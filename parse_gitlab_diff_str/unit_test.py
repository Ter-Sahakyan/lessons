import unittest

from parse_gitlab_diff_str.gitlab_changed_lines_finder import GitLabChangedLinesFinder


class TestGitLabChangedLinesFinder(unittest.TestCase):

    def test_find_added_changed_lines(self):
        diff_string = """@@ -11,6 +11,7 @@ from .models import ProjectRepoInfo
 from .models import ProjectTaskList
 from .models import Task
 from .models import TaskManagementServer
+from .utils import TaskDeadlineHandler
 from client_hooks.b2b import check_and_run_b2b_task_gitlab_pipline
 from client_hooks.b2b import check_gitlab_pipeline_job_statuses
 from hr.models import Employee

@@ -223,3 +223,14 @@ def archive_deleted_tasks():
     )
     for project in projects:
         archive_deleted_tasks_for_project.delay(project.id)
+
+
+@app.task
+def employees_deadline_handler_task():
+    for employee in Employee.objects.all():
+        employee_deadline_handler_task.delay(employee.id)
+
+
+@app.task
+def employee_deadline_handler_task(employee_id):
+    TaskDeadlineHandler(Employee.objects.get(id=employee_id)).handle()"""

        added, removed = GitLabChangedLinesFinder.find_changed_lines(diff_string)

        self.assertEqual(added, [{'end': 14, 'start': 14}, {'end': 236, 'start': 226}])
        self.assertEqual(removed, [])

    def test_find_removed_changed_lines(self):
        diff_string = """@@ -14,7 +14,7 @@ register = template.Library()
 
-@register.simple_tag
-def user_is_active(request) -> bool:
-    if request.user.is_superuser or not request.user.needs_time_tracking:
-        return True
-    last_session = UserActivity.objects.filter(
-        user=request.user,
"""
        added, removed = GitLabChangedLinesFinder.find_changed_lines(diff_string)

        self.assertEqual(added, [])
        self.assertEqual(removed, [{'end': 20, 'start': 15}])

    def test_find_removed_and_added_changed_lines(self):
        diff_string = """@@ -11,6 +11,7 @@ from django.db.models import Subquery
from django.db.models import Sum
from django.db.models import Value
from django.db.models import When
+from django.db.models.functions import Round
from django.db.models.functions import TruncDate
from polymorphic.query import PolymorphicQuerySet

@@ -353,26 +354,27 @@ class TasksQueryset(models.QuerySet):
     ).count() > max_task_under_review_counter
     # or qs.filter(task_list__needs_code_review=False).count() > 3

-    def with_total_actual_hours(self, user):
-        from tasks.models import TaskLifecycle
-        time_spent = Subquery(
-            TaskLifecycle.objects.filter(
-                task=OuterRef('pk'),
-                # author_employee=user.employee,
-                to_list__is_development_started=True,
-            ).values('task__pk').annotate(
-                _total=Sum('actual_time'),
-            ).values('_total'),
-        )
+    def with_total_actual_hours(self, employee):
+        # TODO (task id 2701) rename annotated field to _total_actual_hours
         return self.annotate(
-            _total_actual_time=time_spent,
+            _total_actual_time=Round(
+                Sum(
+                    'lifecycles__actual_time',
+                    filter=Q(
+                        lifecycles__author_employee=employee,
+                        lifecycles__to_list__is_development_started=True
+                    )
+                ) / 60.0,
+                1
+            )
         )

     def with_estimated_hours(self, employee):
         from tasks.models import TaskRole, EmployeeProject
         estimated_hours = Subquery(
             TaskRole.objects.filter(
-                task_id=OuterRef('id'), role=EmployeeProject.objects.filter(
+                task_id=OuterRef('id'),
+                role=EmployeeProject.objects.filter(
                 employee=employee, project=OuterRef('task__project'),
             ).values('role_id')[:1],
         ).values('estimated_hours')[:1],
@@ -445,7 +447,7 @@ class EstimatedHourQueryset(models.QuerySet):
     ).annotate(
         _date=TruncDate('tasklifecycle__updated_at'),
     ).distinct().annotate(
-        _total_actual_time=Sum('tasklifecycle__actual_time') / 60,
+        _total_actual_time=Round(Sum('tasklifecycle__actual_time') / 60.0, 1),
     ).order_by('-_date')"""

        added, removed = GitLabChangedLinesFinder.find_changed_lines(diff_string)

        self.assertEqual(
            added,
            [
                {'start': 14, 'end': 14},
                {'start': 357, 'end': 358},
                {'start': 360, 'end': 369},
                {'start': 376, 'end': 377},
                {'start': 450, 'end': 450},
            ]
        )
        self.assertEqual(
            removed, [
                {'start': 356, 'end': 366},
                {'start': 368, 'end': 368},
                {'start': 375, 'end': 375},
                {'start': 448, 'end': 448},
            ]
        )
