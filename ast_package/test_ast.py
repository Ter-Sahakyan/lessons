import ast


class RewriteName(ast.NodeTransformer):

    def visit_FunctionDef(self, node):
        node.name = 'data'
        node.args.args[0].arg = '*args'
        node.body[0].value.id = 'args'
        return node


if __name__ == '__main__':
    with open('test_file.py') as f:
        code = f.read()

    node = ast.parse(code)
    new_tree = ast.fix_missing_locations(RewriteName().visit(node))
    new_code = ast.unparse(new_tree)
    with open('test_file.py', 'w') as file:
        file.write(new_code)
