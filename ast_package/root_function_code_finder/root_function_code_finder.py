import ast


class FunctionCodeFinder:
    
    def __init__(self, code):
        self.tree = ast.parse(code)
    
    def get_root_method_code_of_line(self, target_line) -> str:
        for node in ast.walk(self.tree):
            if isinstance(node, ast.FunctionDef) and node.lineno < target_line <= node.end_lineno:
                return ast.unparse(node)


if __name__ == '__main__':
    with open('script.py') as f:
        code = f.read()

    print(FunctionCodeFinder(code).get_root_method_code_of_line(6))
