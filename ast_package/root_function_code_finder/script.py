def outer_function():
    def nested_function():
        print('nested function')


def another_function():
    print('another function')


class A:

    def class_method(self):
        print('class method')

    def outer_class_method(self):
        def nested_function():
            print('nested function')

    @property
    def class_property(self):
        return 1
