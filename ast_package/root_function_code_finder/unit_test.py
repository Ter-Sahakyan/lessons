import unittest

from ast_package.root_function_code_finder.root_function_code_finder import FunctionCodeFinder


class TestGetRootMethodCodeOfLine(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open('script.py') as f:
            code = f.read()
        cls.ast_instance = FunctionCodeFinder(code)

    def test_line_inside_the_outer_function(self):
        result = self.ast_instance.get_root_method_code_of_line(3)
        expected = "def outer_function():\n\n    def nested_function():\n        print('nested function')"
        self.assertEqual(result, expected)

    def test_line_inside_the_inner_function(self):
        result = self.ast_instance.get_root_method_code_of_line(7)
        expected = "def another_function():\n    print('another function')"
        self.assertEqual(result, expected)

    def test_line_inside_the_class_method(self):
        result = self.ast_instance.get_root_method_code_of_line(13)
        expected = "def class_method(self):\n    print('class method')"
        self.assertEqual(result, expected)

    def test_line_inside_the_outer_class_method(self):
        result = self.ast_instance.get_root_method_code_of_line(17)
        expected = "def outer_class_method(self):\n\n    def nested_function():\n        print('nested function')"
        self.assertEqual(result, expected)

    def test_line_inside_the_class_property(self):
        result = self.ast_instance.get_root_method_code_of_line(21)
        expected = '@property\ndef class_property(self):\n    return 1'
        self.assertEqual(result, expected)

    def test_line_outside_any_function(self):
        result = self.ast_instance.get_root_method_code_of_line(1)
        self.assertIsNone(result)
