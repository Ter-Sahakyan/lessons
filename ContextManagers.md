# Context Managers

```python
class ExampleClass:

	def __enter__(self):
		return 'entered'

	def __exit__(self, exc_type, exc_val, exc_tb):
		print('exited')
		return 'exited'


def example_function(a, b):
	with ExampleClass() as b:
		print(b)
```