from playwright.sync_api import Playwright, sync_playwright, expect


def test_run(playwright: Playwright) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0")
    page.get_by_title("Обсуждение основной страницы [alt-shift-t]").click()
    page.get_by_placeholder("Искать в Википедии").click()
    page.get_by_placeholder("Искать в Википедии").fill("boo")
    page.get_by_role("button", name="Перейти").click()
    page.locator("b").click()
    page.get_by_label("Содержание").locator("label").click()
    page.get_by_label("Содержание").click()

    # ---------------------
    context.close()
    browser.close()


with sync_playwright() as playwright:
    test_run(playwright)
