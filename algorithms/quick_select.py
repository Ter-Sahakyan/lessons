import random


def quick_select(arr, start, end, k):
    if end - start <= 1:
        return
    rand_ind = random.randint(start, end - 1)
    arr[rand_ind], arr[start] = arr[start], arr[rand_ind]
    pivot_ind = partition(arr, start, end)
    if pivot_ind < k:
        return quick_select(arr, pivot_ind, end, k)
    if pivot_ind > k:
        return quick_select(arr, start, pivot_ind, k)
    return arr[k]


def partition(arr, start, end):

    if end - start <= 1:
        return
    i1 = start + 1
    i2 = end - 1
    while i1 <= i2:
        while i1 <= i2 and arr[i1] <= arr[start]:
            i1 += 1

        while i1 <= i2 and arr[i2] > arr[start]:
            i2 -= 1
        if i1 <= i2:
            arr[i1], arr[i2] = arr[i2], arr[i1]

    arr[start], arr[i2] = arr[i2], arr[start]
    return i2


if __name__ == '__main__':
    arr1 = [1, 5, 6, 2, 7, 10]
    k1 = quick_select(arr1, 0, len(arr1), 2)
    print(arr1)
    print(k1)
