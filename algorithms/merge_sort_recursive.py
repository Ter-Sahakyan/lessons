import datetime


def merge(arr1: list, arr2: list) -> list:
    i1 = i2 = 0
    sorted_arr = []
    while i1 < len(arr1) and i2 < len(arr2):
        if arr2[i2] > arr1[i1]:
            sorted_arr.append(arr1[i1])
            i1 += 1
        else:
            sorted_arr.append(arr2[i2])
            i2 += 1
    sorted_arr.extend(arr2[i2:])
    sorted_arr.extend(arr1[i1:])
    return sorted_arr


def merge_sort(arr: list) -> list:
    if len(arr) <= 1:
        return arr
    else:
        half = len(arr) // 2
        return merge(
            merge_sort(arr[:half]),
            merge_sort(arr[half:]),
        )


if __name__ == '__main__':
    dt = datetime.datetime.now()
    arr_1 = [250, 111, 45, 1, 8, 4, 14, 5, 61, 3, 44, 26, 78, 100]
    print(merge_sort(arr_1))
    print(datetime.datetime.now() - dt)
