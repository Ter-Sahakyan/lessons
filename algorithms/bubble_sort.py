def bubble_sort(arr):
    n = len(arr)
    for i in range(n - 1):
        for j in range((n - 1) - i):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr


if __name__ == '__main__':
    arr_1 = [64, 34, 25, 12, 22, 11, 90, 5, 48]
    print(bubble_sort(arr_1))
