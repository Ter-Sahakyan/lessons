import datetime
from typing import List


class GaussActions:

    def __init__(self, matrix: List[List[float]]):
        self.matrix = matrix
        self._col_len = len(self.matrix[0])
        self._row_len = len(self.matrix)
        self.result = []

    def change_lines(self, index_1: int, index_2: int):
        (self.matrix[index_1], self.matrix[index_2]) = self.matrix[index_2], self.matrix[index_1]

    def mul_line(self, index: int, value: float):
        row = self.matrix[index].copy()
        for i in range(self._col_len):
            row[i] *= value
        return row

    def add_the_lines(self, row1, row2):
        """  line on row1 will change """
        for x in range(self._col_len):
            row1[x] += row2[x]


class GaussMethod(GaussActions):

    def __call__(self):
        col = 0
        while col < self._col_len - 1:
            top_row = col
            if self.lines_sequence(top_row, col):
                row = top_row + 1
                while row < self._row_len:
                    row1 = self.mul_line(top_row, -(self.matrix[row][col] / self.matrix[top_row][col]))
                    self.add_the_lines(self.matrix[row], row1)
                    row += 1
            col += 1
        self.count_result()
        return self.result_var

    @property
    def result_var(self):
        res = ''
        length = len(self.result)
        for i in reversed((range(length))):
            res += f'x{length-i} = {self.result[i]} \n'
        return res

    def __str__(self):
        return ''.join([f'{x}\n' for x in self.matrix])

    def lines_sequence(self, start_row, col) -> bool:
        for row in range(start_row, self._row_len):
            if abs(self.matrix[row][col]) < 0.00001:
                row += 1
            else:
                self.change_lines(start_row, row)
                return True
        return False

    def count_result(self):
        var_col = self._col_len - 2
        row = self._row_len - 1
        print(self)
        while row >= 0:
            col = self._col_len - 2
            value = self.matrix[row][self._col_len - 1]
            for x in self.result:
                value += -(self.matrix[row][col] * x)
                col -= 1
            if abs(self.matrix[row][var_col]) < 0.00001:
                if abs(value) < 0.00001:
                    raise Exception('Infinitely many solutions.')
                raise Exception(f'No solution because {self.matrix[row][var_col]} ≠ {value}')
            self.result.append(value / self.matrix[row][var_col])
            row -= 1
            var_col -= 1


if __name__ == '__main__':
    m = [
        [4, 4, 6, 20],
        [2, 2, 0, 8],
        [2, 2, 5, 6],
    ]
    m1 = [
        [1, 0, -1, 3],
        [2, 1, -2, 11],
        [0, 0, 0, -1],
    ]
    m2 = [
        [2, 1, 7],
        [4, 4, 16],
    ]
    gauss_method = GaussMethod(m)
    print(gauss_method())


