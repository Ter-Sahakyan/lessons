mylist = [1, [3, [[123, 2, 3, 5]], 56], 4]
mylist2 = [8, 9, 6, [5, 8, [9]], 9]
simpl_list = []


def simplification(arr: list):
    for element in arr:
        if type(element) is list:
            simplification(element)
        else:
            simpl_list.append(element)
    return simpl_list


if __name__ == '__main__':
    print(simplification(mylist))
