import random


def quick_sort(arr, start, end):
    if end - start <= 1:
        return
    rand_ind = random.randint(start, end - 1)
    arr[rand_ind], arr[start] = arr[start], arr[rand_ind]
    pivot_ind = partition(arr, start, end)
    quick_sort(arr, start, pivot_ind)
    quick_sort(arr, pivot_ind + 1, end)


def partition(arr, start, end):

    if end - start <= 1:
        return
    i1 = start + 1
    i2 = end - 1
    while i1 <= i2:
        while i1 <= i2 and arr[i1] <= arr[start]:
            i1 += 1

        while i1 <= i2 and arr[i2] > arr[start]:
            i2 -= 1
        if i1 <= i2:
            arr[i1], arr[i2] = arr[i2], arr[i1]

    arr[start], arr[i2] = arr[i2], arr[start]
    return i2


if __name__ == '__main__':
    arr1 = [10, 1, 6, 2, 7, 10, 14, 12, 3, 12]
    arr2 = [2, 1]
    quick_sort(arr1, 0, len(arr1))
    print(arr1)
