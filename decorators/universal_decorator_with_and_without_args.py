import datetime
import time
from functools import wraps

"""
This type of decorator is the generic side of given arguments, it can be used with or without arguments.
"""


class Foo:
    def __init__(self, some_argument):
        self.some_argument = some_argument

    def __call__(self, func):
        @wraps(func)
        def inner(*args, **kwargs):
            start_time = datetime.datetime.now()
            result = func(*args, **kwargs)
            print(datetime.datetime.now() - start_time)
            return result
        return inner


def custom_decorator(func=None, *, some_argument=None):
    dec_maker = Foo(some_argument=some_argument)
    if func:
        return dec_maker(func)
    return dec_maker


@custom_decorator
def foo(a, b):
    time.sleep(2)
    return a + b


@custom_decorator(some_argument=78)
def foo_1(a, b):
    time.sleep(2)
    return a + b


if __name__ == '__main__':
    foo(5, 6)
    foo_1(5, 6)
