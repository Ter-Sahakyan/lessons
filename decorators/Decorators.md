# Decorators

## Decorators without parameters

```python
def example_fuc_dec(func):
	def wrapper(*args, **kwargs):
		print('start')
		func(*args, **kwargs)
		print('end')
	return wrapper


@example_fuc_dec
def example_function(a, b):
	print(a * b)

# How it's working underneath
multiply_together = example_fuc_dec(example_function)
```

## Decorators with parameters

```python
def example_fuc_dec(num, num_1):
	def wrapper_1(func):
		def wrapper_2(*args, **kwargs):
			print('start')
			func(*args, **kwargs)
			print('end')
		return wrapper_2
	return wrapper_1


@example_fuc_dec(1, 2)
def example_function(a, b):
	print(a * b)

# How it's working underneath
example_function = example_fuc_dec(1, 2)(example_function)
```


## Class decorators without parameters

```python
class ExampleClass:
	def __init__(self, arg):
		self._arg = arg

	def __call__(self, a, b):
		print('start')
		self._arg(a, b)
		print('end')

@ExampleClass
def example_function(a, b):
	print(a * b)

# How it's working underneath
example_function = ExampleClass(example_function)
```

## Class decorators with parameters

```python
class ExampleClass:
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __call__(self, func):
		def wrapper(*args, **kwargs):
			print('start')
			func(self.x, self.y)
			print('end')
		return wrapper


@ExampleClass(1, 2)
def example_function(a, b):
	print(a * b)
    
# How it's working underneath
example_function = ExampleClass(1, 2)(example_function)
```
