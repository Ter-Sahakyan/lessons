from decorators.universal_decorator_with_and_without_args import custom_decorator


class A:

    def __init__(self, number):
        self.number = number

    @property
    @custom_decorator
    def foo(self):
        return f'--- {self.number} ---'


if __name__ == '__main__':
    a = A(5)
    print(a.foo)
