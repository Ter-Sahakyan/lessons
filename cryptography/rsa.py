import datetime

from cryptography.key_generator import RSAKeyGenerator, RSAKey


class RSA:
    def __init__(self, public_key: RSAKey, private_key: RSAKey):
        self.public_key = public_key
        self.private_key = private_key

    def encrypt(self, plaintext):
        ciphertext_list = [(ord(x) ** self.public_key.exp) % self.public_key.mod for x in plaintext]
        return ' '.join(map(str, ciphertext_list))

    def decrypt(self, ciphertext):
        plaintext_list = [chr((int(x) ** self.private_key.exp) % self.private_key.mod) for x in ciphertext.split()]
        return ''.join(plaintext_list)


if __name__ == '__main__':
    start_date = datetime.datetime.now()
    key_generator = RSAKeyGenerator()
    keys = key_generator.generate_keys()
    print(keys, key_generator.f_euler)
    rsa = RSA(*keys)
    ciphertext_1 = rsa.encrypt('Hello world...')
    print(ciphertext_1)
    plaintext_1 = rsa.decrypt(ciphertext_1)
    print(plaintext_1)
    print(datetime.datetime.now() - start_date)
