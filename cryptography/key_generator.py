import datetime
import random
import dataclasses


@dataclasses.dataclass
class RSAKey:
    exp: int
    mod: int


class RSAKeyGenerator:
    random_prime_range_start = 3
    random_prime_range = 100

    def __init__(self):
        self.q = self.get_random_prime()
        self.p = self.get_random_prime()
        self.mod = self.q * self.p
        self.f_euler = (self.q - 1) * (self.p - 1)

    def find_primes(self, start, limit):
        primes = []
        is_prime = [True] * (limit + 1)
        is_prime[0] = is_prime[1] = False

        for i in range(2, int(limit ** 0.5) + 1):
            if is_prime[i]:
                for j in range(max(i * i, (start // i) * i), limit + 1, i):
                    is_prime[j] = False

        for i in range(max(2, start), limit + 1):
            if is_prime[i]:
                primes.append(i)

        return primes

    def get_random_prime(self, limit=None):
        if limit is None:
            limit = self.random_prime_range
        start = random.randint(self.random_prime_range_start, limit)
        primes = self.find_primes(start, limit)
        if not primes:
            return self.get_random_prime(limit)
        return random.choice(primes)

    def get_public_key_exponent(self):
        return self.get_random_prime(self.f_euler)

    def get_private_key_exponent(self, public_key_exponent):
        return pow(public_key_exponent, -1, self.f_euler)

    def generate_keys(self):
        while True:
            public_key_exponent = self.get_public_key_exponent()
            try:
                private_key_exponent = self.get_private_key_exponent(public_key_exponent)
            except ValueError as e:
                print(e)
            else:
                return RSAKey(public_key_exponent, self.mod), RSAKey(private_key_exponent, self.mod)


if __name__ == '__main__':
    start_date = datetime.datetime.now()
    print(RSAKeyGenerator().generate_keys())
    print(datetime.datetime.now() - start_date)
