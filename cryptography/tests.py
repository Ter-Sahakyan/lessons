from cryptography.key_generator import RSAKeyGenerator


def is_prime(number):
    if number < 2:
        return False
    for i in range(2, int(number**0.5) + 1):
        if number % i == 0:
            return False
    return True


def test_rsa_generator():
    for i in range(10000):
        key_generator = RSAKeyGenerator()
        public_key, private_key = key_generator.generate_keys()
        assert is_prime(public_key.exp)
        assert public_key.exp < key_generator.f_euler
        assert (public_key.exp * private_key.exp) % key_generator.f_euler == 1
